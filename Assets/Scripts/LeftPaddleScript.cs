
using UnityEngine;

public class LeftPaddleScript : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private GameObject ball;

    private bool aiMode;


    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(-12, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
        if(!aiMode)
        {
            if(transform.position.y < 11 && Input.GetKey(KeyCode.W))
            {
                transform.position += Vector3.up * moveSpeed * Time.deltaTime;
            }
            
            if(transform.position.y > -11 && Input.GetKey(KeyCode.S))
            {
                transform.position -= Vector3.up * moveSpeed * Time.deltaTime;
            }
        }
        else
        {
            if(transform.position.y < 11 && underBall())
            {
                transform.position += Vector3.up * moveSpeed * Time.deltaTime;
            }
            
            if(transform.position.y > -11 && overBall())
            {
                transform.position -= Vector3.up * moveSpeed * Time.deltaTime;
            }
        }
        
    }

    //set flag for single player vs two player
    public void enableTwoPlayerMode()
    {
        aiMode = false;
    }

    //set flag for single player vs two player
    public void enableSinglePlayerMode()
    {
        aiMode = true;
    }

    //logic check for ai movement
    private bool underBall()
    {
        return transform.position.y<ball.transform.position.y;
    }

    //logic check for ai movement
    private bool overBall()
    {
        return transform.position.y>ball.transform.position.y;
    }
}
